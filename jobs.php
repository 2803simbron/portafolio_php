<?php

class Job {
    private $titulo;
    public $descripcion;
    public $visible= true;
    public $meses;

    public function __construct($titulo, $descripcion){
        $this->setTitulo($titulo);
        $this->descripcion = $descripcion;
    }

    public function setTitulo($titu){
        if($titu == ''){
            $this->titulo = 'N/A';
        }else{
            $this->titulo = $titu;
        }
       
    }

    public function getTitulo(){
        return $this->titulo;
    }

    function getDurationAsString(){
        $años = floor($this->meses / 12);
        $extrameses = $this->meses % 12;
    
        if($años == 0){
            return "$extrameses meses";
        }
    
        if ($extrameses == 0){
            return "$años Años ";
        }
    
        if($años != 0 && $extrameses != 0 ){
            return "$años Años $extrameses meses";
        }
        
    }
}

$job1 = new Job('PHP Desarrollador','Para desarrollo web e interaccion con el usuario');

$job1->meses = 66;

$job2 = new Job('Python Desarrollador','Para desarrollo marching learning');

$job2->meses = 15;

$job3 = new Job('Java desarrollador','Para desarrollo Java learning');

$job3->meses = 36;

$jobs = [
    $job1,
    $job2,
    $job3
    // [
    //     'titulo' => 'PHP Desarrollador',
    //     'descripcion' => 'Para desarrollo web e interaccion con el usuario',
    //     'visible' => true,
    //     'meses' => 66
    // ],
    // [
    //     'titulo' => 'Python Desarrollador',
    //     'descripcion' => 'por medio de framework Django es muy robusto y potente',
    //     'visible' => true,
    //     'meses' => 44
    // ],
    // [
    //     'titulo' => 'Devops',
    //     'descripcion' => 'Es un trabajo increible!!!!',
    //     'visible' => true,
    //     'meses' => 55
    // ],
    // [
    //     'titulo' => 'JAVA Desarrollador',
    //     'descripcion' => 'Es portable y su seguridad lo hace el mas seguro',
    //     'visible' => true,
    //     'meses' => 24
    // ],
    // [
    //     'titulo' => 'Administrador de Bases de Datos',
    //     'descripcion' => 'Partiendo de Bases de Datos con Mysql Postgrade MongoDB',
    //     'visible' => true,
    //     'meses' => 3
    // ],
    // [
    //     'titulo' => 'Administrador de Servidores Linux',
    //     'descripcion' => 'Los mas utilizados en la industria CentOs Ubuntu kaly',
    //     'visible' => true,
    //     'meses' => 7
    // ],
    // [
    //     'titulo' => 'Soporte Profesional a Tecnologias de informacion',
    //     'descripcion' => 'Desde impantacion de una red y Mantenimiento de equipo de computo',
    //     'visible' => true,
    //     'meses' => 9
    // ]
];



function pintJob($job)
 {
    if ($job->visible == false)
    {
        return;
    }
    echo '<li class="work-position">';
    echo '<h5>'.$job->getTitulo().'</h5>';
    echo '<p>'.$job->descripcion.'</p>';
    echo '<p>'.$job->getDurationAsString().'</p>';
    echo '<strong>Achievements:</strong>';
    echo '<ul>';
    echo ' <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo ' <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo ' <li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo '</ul>';
    echo '</li>';
                                  
}

 //$var1 =3;
    //if($var1 > 2){
    //    echo 'es mayor que 2';
    //} else {
     //   echo 'No mayor que 2';
    //}
